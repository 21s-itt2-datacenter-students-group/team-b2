# Team B2

### Team B2 ITT2 datacenter project
Be mindful of branches - the team has started to utilize the powers of Git.

#### Members consist of:
- Aleksis Kairiss,
- Alexander Bjørk Andersen,
- Daniel Rasmussen,
- Jacob Suurballe Petersen,
- Nikolaj Hult-Christensen,
- Sheriff Dibba

#### External Links

<a href="https://padlet.com/dara28918/pphbz6u2qo3eflvt">Brainstorm for the 10th of February 2021</a><br>
<a href="https://docs.google.com/document/d/1p7Q4kVMKAJkqSQ1qQKQRLiMmJr5so1wH/edit#">Global Group Contract with team B2, A2, A4 and the OME's</a><br>
<a href="https://docs.google.com/document/d/1ikNSmcDvUXBDG2OOOH5QhJ42Xr-Rk4ZkOU1OGKem67s/edit?usp=sharing">Google Document: "How to measure current/cooling/ventilation"</a><br>
<a href="https://docs.google.com/document/d/1tfGMAO5aoPtzDC3XvHSrtsSoa8Yma-qmxCR7Ux6-49U/edit?usp=sharing">Scrum master, facilitator and secretary </a><br>
<a href="https://www.ug.dk/uddannelser/professionsbacheloruddannelser/tekniskeogteknologiskeudd/maritimeuddannelser/maskinmester">OME Education </a><br>
<a href="https://youtu.be/qDMYbmVYmlg">PoC video</a><br>
<a href="https://docs.google.com/document/d/1MQrLGzpiYdRu7KKnLWX8uMiRR9WGdWwNrJKlAWoCyns/edit?usp=sharing">System robustness documentation </a><br>
<a href="https://docs.google.com/document/d/1d_mhsnX0nIMlcuB41m_z5D-NQV1U-6eBv3ul-106JR0/edit?usp=sharing">MQTT Security documentation </a>

#### Internal Links

<a href="https://gitlab.com/21s-itt2-datacenter-students-group/team-b2/-/blob/Cleanup/Use_cases.pdf">Use case Diagram</a><br>
<a href="https://gitlab.com/21s-itt2-datacenter-students-group/team-b2/-/blob/Cleanup/Block%20diagram.pdf">system overview</a><br>
<a href="https://gitlab.com/21s-itt2-datacenter-students-group/team-b2/-/blob/master/Pre-Mortem.md">Pre-Mortem</a><br>
<a href="https://gitlab.com/21s-itt2-datacenter-students-group/team-b2/-/blob/master/Exercises/How_to_measure_current_cooling_ventilation.pdf">How to measure current/cooling/ventilation</a><br>
<a href="https://gitlab.com/21s-itt2-datacenter-students-group/team-b2/-/blob/master/requirementDraft.md">Requirements draft</a><br>
<a href="https://gitlab.com/21s-itt2-datacenter-students-group/team-b2/-/blob/master/Exercises/sensor_requirements.pdf">Sensor requirements</a><br>
<a href="https://gitlab.com/21s-itt2-datacenter-students-group/team-b2/-/blob/master/Exercises/Sensors.md">Suitable sensors</a><br>
<a href="https://gitlab.com/21s-itt2-datacenter-students-group/team-b2/-/blob/master/branchingGuide.md">Branching Guide</a><br>
<a href="https://gitlab.com/21s-itt2-datacenter-students-group/team-b2/-/blob/master/gitGuidelines.md">Git Guidelines</a><br>
<a href="https://gitlab.com/21s-itt2-datacenter-students-group/team-b2/-/blob/master/Exercises/publish_potentiometer.py">MQTT publish_potentiometer python file</a><br>
<a href="https://gitlab.com/21s-itt2-datacenter-students-group/team-b2/-/blob/master/Exercises/publish_tempC.py ">MQTT publish_tempC python file</a><br>
<a href="https://gitlab.com/21s-itt2-datacenter-students-group/team-b2/-/milestones">Milestones</a><br>
<a href="https://gitlab.com/21s-itt2-datacenter-students-group/team-b2/-/blob/master/Exercises/dataPersistenceUsingMongoDB.md">Data persistence using MongoDB</a><br>
<a href="https://gitlab.com/21s-itt2-datacenter-students-group/team-b2/-/blob/projectPlan/projectPlan.md">Milestones</a><br>
<a href="https://gitlab.com/21s-itt2-datacenter-students-group/team-b2/-/blob/master/minimumViableProduct.md">Minimum Viable Product</a>
