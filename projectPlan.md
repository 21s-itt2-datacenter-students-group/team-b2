---
title: '21S ITT2 Project'
subtitle: 'Project plan, part I'
authors: ['Nikolaj Simonsen \<nisi@ucl.dk\>', 'Mathias Gregersen \<megr@ucl.dk\>']
main_author: 'Nikolaj Simonsen'
date: \today
email: 'nisi@ucl.dk'
left-header: \today
right-header: Project plan
skip-toc: false
skip-tof: true
---

# Background

This is the semester ITT2 project where we will work with measuring power data in a data center. This project plan will cover the project, and will match topics that are taught in parallel classes.

The overall project case is described at [https://datacenter2.gitlab.io/datacenter-web/](https://datacenter2.gitlab.io/datacenter-web/)

# Purpose

The main goal is to have a system where IoT sensors collect data from a data center.
This is a learning project and the primary objective is for the students to get a good understanding of the challenges involved in making such a system and to give them hands-on experience with the implementation.

For simplicity, the goals stated wil evolve around the technical project goals. It must be read using the weekly plan as a complementary source for the *learning* oriented goals.

# Goals

The overall system that is going to be build looks as follows:  
![project_overview](../docs/datacenter_iot_system_overview.png "ITT2 project overview")  
Reading from the left to the right:  

* Sensor modules 1-3: A placeholder for x number of sensors
* Raspberry Pi: The embedded system to run the sensor software and to be the interface to the MQTT broker.  
* Computer: Connects to both the Raspberry Pi and Gitlab while developing and troubleshooting  
* MQTT broker: MQTT allows for messaging between device to cloud and cloud to device. This makes for easy broadcasting messages to groups of things. 
* Consumer: 


Project deliveries are:  

* A system reading and writing data to and from the sensors/actuators  
* Docmentation of all parts of the solution  
* Regular meeting with project stakeholders.  
* Final evaluation 
* TBD: Fill in more, as agreed, with teams from the other educations


# Schedule

See the [lecture plan](https://eal-itt.gitlab.io/21s-itt2-project/other-docs/21S_ITT2_PROJECT_lecture_plan.html) for details.

# Organization

[Considerations about the organization of the project may includes  
    • Identification of the steering committee  
    • Identification of project manager(s)  
    • Composition of the project group(s)  
    • Identification of external resource persons or groups  
    	OME  


# Budget and resources

Small monetary resources are expected. In terms of manpower, only the people in the project group are expected to contribute.


# Risk assessment

1. The epics were not cut down enough, they were too ambitious. 
2. We missed deadlines, and failed to deliver on time. We took on too small/big tasks, and went over our timelimits for those individual projects/tasks.
3. Team members did not show up for some unknown reason on several occasions. That resulted in lack of knowledge which were required to move forward.
4. The team lacked engagement, and were not passionate enough.
5. The Corona-virus came in the way. There were restrictions and limitations, and team members were infected and unavailable.
6. Overconfidence. The team did not think the tasks through, and thought tasks were easier than they were.
7. Bad communication between team members. They ended up having heated arguments and talked past each other.
8. We had hardware malfunction.
9. We ended up making our things more complicated than they had to be.
10. After the project were done, we forgot to hand it in. We kept on working on it afterwards, trying to refine it over and over.
11. The team did not keep up with team meetings and planning.
12. One or more from the team dropped out, leaving the team thinned.
13. Internet services we were dependent on crashed, and we lost access to them.
14. The tasks were not clear enough, which ended up having a different result than expected.


# Stakeholders

OME  
Data center  
Teachers  
Other students



# Communication

OME - Discord  
Students - Discord  
Teachers - Discord/Element  
Data center - Dont know yet  


# Perspectives

This project will serve as a template for other similar projects in future semesters.

It can also serve as an example of the students abilities in their portfolio, when applying for jobs or internships.


# Evaluation

[Evaluation is about how to gauge is the project was successful. This includes both the process and the end result. Both of which may have consequences on future projects.  
The will be some documentation needed at the end (or during) the project, e.g. external funding will require some sort of reporting after the project has finished. This must be described.]  

TBD: Students fill out this one also

# References

[Include any relevant references to legal documents, literature, books, home pages or other material that is relevant for the reader.]

TBD: Students may add stuff here at their discretion


* Common project group on gitlab.com

  The project is managed using gitlab.com. The groups is at [https://gitlab.com/20s-itt-dm-project](https://gitlab.com/20s-itt-dm-project)
