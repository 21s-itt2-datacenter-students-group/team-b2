# Data persistence using MongoDB  
  
### Setup
* Azure virtual machine 1 - Funtioning as mqtt broker
* Azure virtual machine 2 - Functioning as subscriber and sending data to mongoDB
* MongoDB
* Raspberry Pi - Functioning as publisher  
  
#### Azure virtual machine 1
Install mqtt software, we use mosquitto.  
`apt install mosquitto`  
`systemctl enable mosquitto`  
In Azure open port 1883, inbound and outbound.  
Set dns name.  
  
#### Azure virtual machine 2  
Install python 3.
Use pip to install dependent packages:
`pip3 install paho-mqtt pymongo dnspython`
In Azure open ports 1883 and 27017, inbound and outbound.
  
#### MongoDB  
Allow "Azure vm2" ip address.  
  
#### Raspberry Pi  
Setup a mqtt publisher.  
Send to dns address setup on Azure VM1 (broker).