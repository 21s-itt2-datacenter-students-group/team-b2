# Minimum Viable Product  
  
## Features
Our MVP will be able to measure voltage using an analog input, and give an alert when voltage dips below X voltage.  
  
## Purpose
To get feedback on what is actually wanted/needed in the final product.  
To not waste time on unnecessary features.  
To get the product to the customer ASAP.
