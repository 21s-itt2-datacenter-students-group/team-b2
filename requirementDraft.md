B2:
It has now been established that B2 will during this project work on these issues:
Voltage variance
Voltage harmonics
Frequency
Voltage unbalance
Nice to have, not a hard requirement: Flicker

B2 will be monitoring the issues above on both 400V and 230V power lines. While OME were a little uncertain where in the datacenter the monitoring systems would be placed, the following is to be assumed: 400V = grid to datacenter, 230V = One system outside each server room.
All requirements to the system is present in the file provided by OME's: https://cdn.discordapp.com/attachments/814108675504865300/816634735006187540/unknown.png

OME:
Will follow up on placement of the system, will tell if it deviates from expectations.
